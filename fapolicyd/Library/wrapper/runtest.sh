#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Project url: https://gitlab.cee.redhat.com/SELinux/test-wrapper
#   Description: a wrapper which runs given upstream test
#   Author: Petr Lautrbach <plautrba@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2019 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

set -ex

test -n "$TEST_GIT_URL"
test -n "$TEST_GIT_PATH"

TEST_GIT_REF="${TEST_GIT_REF:-"master"}"
TEST_GIT_SCRIPT="${TEST_GIT_SCRIPT:-"./runtest.sh"}"

TEST_DIR="$(mktemp -d test-repo.XXXXXXXX)"

trap 'rm -rf "${TEST_DIR}"' EXIT

git clone "${TEST_GIT_URL}" "${TEST_DIR}"

cd "${TEST_DIR}"

git checkout "${TEST_GIT_REF}"

cd "${TEST_GIT_PATH}"

chmod +x "${TEST_GIT_SCRIPT}"
"${TEST_GIT_SCRIPT}"
